import React from "react";
import "./cusomButton.scss";
export const CustomButton = ({ signIn, children, ...otherPropsButtons }) => {
  return (
    <div
      className={`${signIn ? "signIn" : ""} custom-button`}
      {...otherPropsButtons}
    >
      {children}
    </div>
  );
};
