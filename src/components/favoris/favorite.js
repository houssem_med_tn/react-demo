import React from "react";
import { useSelector } from "react-redux";
import { createStructuredSelector } from "reselect";
import { selectFavoris } from "../../redux/user/userSelector";
import UserComponent from "../user-component/UserComponent";
import "./favoris.scss";
export const Favoris = () => {
  const { filtered } = useSelector(
    createStructuredSelector({
      filtered: selectFavoris,
    })
  );

  return (
    <div className="favoris">
      {filtered &&
        filtered.map((u) => {
          return <UserComponent btn filter hidden key={u.id} user={u} />;
        })}
    </div>
  );
};
