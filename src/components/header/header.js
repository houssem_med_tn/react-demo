import React from "react";
import "./header.scss";
import { Link } from "react-router-dom";
import Logo from "../../assets/logomed.png";
import { useSelector, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";
import { currentUser } from "../../redux/user/userSelector";
import { setCurrentUser } from "../../redux/user/userActions";
const Header = () => {
  const { user } = useSelector(
    createStructuredSelector({
      user: currentUser,
    })
  );

  const dispatch = useDispatch();
  return (
    <div className="header">
      <Link className="logoContainer" to="/">
        <img src={Logo} className="logo" alt="" />
      </Link>
      <div className="header-items">
        <Link className="favoris" to="/favorites">
          Favourites
        </Link>
        {user ? (
          <button
            className="emailu"
            onClick={() => dispatch(setCurrentUser(null))}
          >
            Sign OUT
          </button>
        ) : (
          <Link className="button" to="/login">
            Se Connecter
          </Link>
        )}

        <hr
          style={{
            width: "1px",
            border: "none",
            height: "20px",
            color: "white",
            backgroundColor: "white",
          }}
        />
        <select className="select-option">
          <option className="option" value="F">
            Francais
          </option>
          <option className="option" value="A">
            Anglais
          </option>
        </select>
      </div>
    </div>
  );
};

export default Header;
