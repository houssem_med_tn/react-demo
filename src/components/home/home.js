import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import "./home.scss";
import UserComponent from "../user-component/UserComponent";
import { fetchAllUsers } from "../../redux/user/userActions";
import { selectAllUser, selectValue } from "../../redux/user/userSelector";
import { createStructuredSelector } from "reselect";
import { Searchbar } from "../../components/searchbar/searchbar";

const HOME = () => {
  var { users } = useSelector(
    createStructuredSelector({
      users: selectAllUser,
      value: selectValue,
    })
  );

  const dispatch = useDispatch();
  const [data, setdata] = useState([]);

  const [searchfield, setSearchField] = useState("");
  useEffect(() => {
    dispatch(fetchAllUsers());
  }, [dispatch]);

  useEffect(() => {
    console.log(`{Config.apiUrl}`);
    setdata(
      users.filter((country) =>
        country.name.toLowerCase().includes(searchfield.toLowerCase())
      )
    );
  }, [searchfield, users]);

  const handleChange = (value) => {
    setSearchField(value);
    // dispatch(filterByName(value));
  };

  return (
    <div className="users-card">
      <Searchbar
        queryt={searchfield}
        handleChange={(e) => handleChange(e.target.value)}
      />
      {data.length ? (
        data.map((u) => {
          return <UserComponent key={u.id} user={u} />;
        })
      ) : (
        <div
          style={{ marginTop: "40px", fontWeight: "bold", fontSize: "17px" }}
        >
          No Users found
        </div>
      )}
    </div>
  );
};

export default HOME;
