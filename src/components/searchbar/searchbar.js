import React from "react";
import "./searchbar.scss";
import Logo_Medecin from "../../assets/ic_medcin.png";
import searchLogo from '../../assets/com.png';
export const Searchbar = ({ handleChange, queryt, search1, search2, search3 }) => {
  return (
    <div className="search-bar">
      <div className="search">
        <div className="search1">
          <div className="logo-cont">
            <img src={Logo_Medecin} alt="" />
          </div>
          <input
            type="search"
            name={search1}
            onChange={handleChange}
            value={queryt}
            className="search-user"
            placeholder="Spécialité"
          />
        </div>
        <hr
        style={{
          margin: '8px',
          width: "1px",
          height: "80%",
          backgroundColor: "black",
          border: "none",
        }}
      />
        <div className="search2">
          <div className="p-container">
            <p>QUI?</p>
            </div>
          <input
            type="search"
            name={search2}
            className="search-user"
          />
        </div>
        <hr
        style={{
          margin: '8px',
          width: "1px",
          height: "80%",
          backgroundColor: "black",
          border: "none"
        }}
      />
        <div className="search3">
        <div className="p-container">
            <p>OU?</p>
            </div>
          <input
            type="search"
            name={search3}
            className="search-user"
          />
        </div>
        <div className='logo-search'>
          <img src={searchLogo} alt=''/>
        </div>
      </div>
    </div>
  );
};
