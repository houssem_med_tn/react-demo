import React, { useState } from "react";
import { CustomButton } from "../../../sharedui/CustomButton/customButtton";
import { FormInput } from "../../../sharedui/FormInput/form-input";
import { useDispatch } from "react-redux";
import { UserAuth } from "../../login/userAuth";
import "./sign-up.scss";
import { setCurrentUser } from "../../../redux/user/userActions";

export const SignUp = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const dispatch = useDispatch();

  const handleChange = (target) => {
    const { value, name } = target;
    if (name === "name") {
      setName(value);
    } else if (name === "email") {
      setEmail(value);
    } else if (name === "password") {
      setPassword(value);
    } else {
      setConfirmPassword(value);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      alert("password does not match");
      return;
    }
    var userAuth = new UserAuth(name, email, password, confirmPassword);
    dispatch(setCurrentUser(userAuth));
    localStorage.setItem("userAuth", JSON.stringify(userAuth));
    alert("user signed up successfully");
    setEmail("");
    setPassword("");
    setName("");
    setConfirmPassword("");
  };

  return (
    <div className="sign-up">
      <h2 className="title">I do not have an account</h2>
      <span>Signup with your email and password</span>
      <form onSubmit={() => handleSubmit()} className="signup-form">
        <FormInput
          type="text"
          value={name}
          name="name"
          label="username"
          required
          handleChange={(e) => handleChange(e.target)}
        />
        <FormInput
          type="email"
          name="email"
          value={email}
          label="email"
          handleChange={(e) => handleChange(e.target)}
          required
        />
        <FormInput
          type="password"
          value={password}
          name="password"
          label="password"
          handleChange={(e) => handleChange(e.target)}
          required
        />
        <FormInput
          type="password"
          name="confirmpassword"
          value={confirmPassword}
          label="confirm password"
          handleChange={(e) => handleChange(e.target)}
          required
        />
        <CustomButton onClick={(e) => handleSubmit(e)} type="submit" signIn>
          Sign UP
        </CustomButton>
      </form>
    </div>
  );
};
