import React from "react";
import "./login.scss";
import { SignIn } from "./sign-in/sign-in";
import { SignUp } from "./sign-up/sign-up";

export const LoginPage = () => {
  return (
    <div className="login-page">
      <SignIn />
      <SignUp />
    </div>
  );
};
