import React, { useState } from "react";
import { CustomButton } from "../../../sharedui/CustomButton/customButtton";
import { FormInput } from "../../../sharedui/FormInput/form-input";
import "./sign-in.scss";
import { useSelector } from "react-redux";
import { createStructuredSelector } from "reselect";
import { currentUser } from "../../../redux/user/userSelector";

export const SignIn = () => {
  const { user } = useSelector(
    createStructuredSelector({
      user: currentUser,
    })
  );
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const handleChange = (target) => {
    const { value, name } = target;
    if (name === "email") {
      setEmail(value);
    } else if (name === "password") {
      setPassword(value);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (user !== undefined) {
      if (email !== user.email || password !== user.password) {
        alert("bad credentials");
      } else {
        alert("user signed in successfully");
      }
    }
  };
  return (
    <div className="sign-in">
      <h2>I already have an account</h2>
      <span className="title">Sign in with your email and password</span>
      <form onSubmit={(e) => handleSubmit(e)}>
        <FormInput
          type="email"
          name="email"
          handleChange={(e) => handleChange(e.target)}
          value={email}
          label="email"
          required
        />
        <FormInput
          type="password"
          name="password"
          value={password}
          label="password"
          handleChange={(e) => handleChange(e.target)}
          required
        />
        <CustomButton type="submit" onClick={(e) => handleSubmit(e)} signIn>
          Sign IN
        </CustomButton>
      </form>
    </div>
  );
};
