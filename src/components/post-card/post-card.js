import React from "react";
import { withRouter } from "react-router";
import { CustomButton } from "../../sharedui/CustomButton/customButtton";
import "./post-card.scss";
const PostCard = ({ id, user, title, body, match, history }) => {
  return (
    <div className="post-card">
      <div className="card-top">
        <div className="post-title">
          <p>Présenation du {user && user.name}</p>
          <span>{title}</span>
          <br />
          <span>{body}</span>
        </div>
      </div>
      <div className="card-bottom">
        <div className="button-right">
          <CustomButton onClick={() => history.push(`/posts/${id}`)}>
            ViewComments
          </CustomButton>
        </div>
      </div>
    </div>
  );
};

export default withRouter(PostCard);
