import React from "react";
import "./UserComponent.scss";
import { withRouter } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  clearItem,
  addUserToListFavorit,
  clearItemFromFavoris,
} from "../../redux/user/userActions.js";
import { CustomButton } from "../../sharedui/CustomButton/customButtton";
const UserComponent = ({ user, history, match, btn, filter, hidden }) => {
  const dispatch = useDispatch("");

  return (
    <div className="user-card">
      <div className="left">
        <div className="top">
          <div className="top-left">
            <div className="image-container">
              <div className="img-container">
                <img
                  className="image"
                  src="https://cdn.pixabay.com/photo/2016/01/19/15/05/doctor-1149149__480.jpg"
                  alt=""
                />
              </div>
              <div className="ouvert">
                <span className="spn1">Ouvert</span>
                <span className="spn">ferme dans 2h</span>
              </div>
            </div>
          </div>
          <div className="top-right">
            <div className="user-info">
              <h3 className="username">{user.name}</h3>
              <span className="emailuser">{user.email}</span>
              <hr />
            </div>

            <div className="buttons">
              {btn ? (
                <CustomButton
                  onClick={() => {
                    history.push(`/users/${user.id}`);
                  }}
                >
                  View Profile
                </CustomButton>
              ) : (
                <CustomButton
                  onClick={() => {
                    history.push(`${match.url}users/${user.id}`);
                  }}
                >
                  View Profile
                </CustomButton>
              )}
              {filter ? (
                <CustomButton
                  onClick={() => dispatch(clearItemFromFavoris(user))}
                >
                  Remove
                </CustomButton>
              ) : (
                <CustomButton onClick={() => dispatch(clearItem(user))}>
                  Clear item
                </CustomButton>
              )}
              {hidden ? null : (
                <CustomButton
                  onClick={() => dispatch(addUserToListFavorit(user))}
                >
                  Add item
                </CustomButton>
              )}
            </div>
          </div>
        </div>
        <div className="bottom">
          <div className="bottom-left">
            <div className="bold">
              <h2>{user.company.name}</h2>
            </div>
            <div className="content">
              <span>{user.address.city}</span>
              <span>{user.phone}</span>
              <span>{user.website}</span>
            </div>
          </div>
          <hr
            style={{
              width: "2px",
              color: "gray",
              height: "80%",
              backgroundColor: "#F2F2F2",
              border: "none",
            }}
          />
          <div className="bottom-right">
            <div className="bold">
              <h2>{user.username}</h2>
            </div>
            <div className="content">
              <span>{user.address.street}</span>
              <span>{user.address.suite}</span>
              <span>{user.address.zipcode}</span>
            </div>
          </div>
        </div>
      </div>
      <hr
        style={{
          width: "1px",
          color: "gray",
          height: "95%",
          backgroundColor: "#F2F2F2",
          border: "none",
        }}
      />
      <div className="right">
        <div className="right-title">
          <span className="span-title">Prendre rendez-vous en ligne</span>
          <span className="span-heure">Choisir une date et heure</span>
        </div>
        <div className="calendar"></div>
      </div>
    </div>
  );
};
export default withRouter(UserComponent);
