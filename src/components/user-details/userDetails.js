import React, { useEffect } from "react";
import "./userDetails.scss";
import { useDispatch, useSelector } from "react-redux";
import { getUserById } from "../../redux/user/userActions";
import { selectUsers } from "../../redux/user/userSelector";
import { createStructuredSelector } from "reselect";
import { fetchPosts } from "../../redux/posts/postAction";
import { selectAllPosts } from "../../redux/posts/postSelector";
import PostCard from "../post-card/post-card";
const UserDe = ({ match }) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const dispatch = useDispatch();
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { user, posts } = useSelector(
    createStructuredSelector({
      user: selectUsers,
      posts: selectAllPosts,
    })
  );
  useEffect(() => {
    dispatch(fetchPosts(match.params.id));
    dispatch(getUserById(match.params.id));
  }, [dispatch, match.params.id]);

  const redirectToQuestions = () => {
    document.querySelector(".user-content").style.visibility = "hidden";
  };

  const redirectToPosts = () => {
    document.querySelector(".user-content").style.visibility = "visible";
  };
  return (
    <div className="profil-user">
      <div className="profil-info">
        <div className="profil-left">
          <div className="image-container">
            <img
              className="img"
              src="https://cdn.pixabay.com/photo/2016/01/19/15/05/doctor-1149149__480.jpg"
              alt=""
            />
          </div>
        </div>
        <div className="profil-right">
          <div className="user-in">
            <div className="container">
              <div className="title">{user && <p>{user.name}</p>}</div>
              <div className="info">
                {user && (
                  <>
                    <span className="span-title">{user.phone}</span>
                    <span>{user.email}</span>
                  </>
                )}
              </div>
            </div>
          </div>
          <div className="select-items">
            <button onClick={() => redirectToPosts()}>Posts</button>
            <button onClick={() => redirectToQuestions()}>Questions</button>
            <button>Vidéos</button>
            <button>Articles</button>
          </div>
        </div>
      </div>
      <div className="user-content">
        <div className="user-posts">
          <div className="user-post-card">
            {posts &&
              posts.map(({ id, ...otherProps }) => (
                <PostCard key={id} {...otherProps} id={id} user={user} />
              ))}
          </div>
        </div>
        <div className="calendar-user"></div>
      </div>
    </div>
  );
};

export default UserDe;
