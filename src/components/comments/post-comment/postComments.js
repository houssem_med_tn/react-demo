import React, { useEffect } from "react";
import "./postComments.scss";
import { withRouter } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import { selectAllComments } from "../../../redux/posts/postSelector";
import { createStructuredSelector } from "reselect";
import { fetchComments } from "../../../redux/posts/postAction";
import { CardComment } from "../comment-card/commentsCard";

const PostComments = ({ match }) => {
  const { comments } = useSelector(
    createStructuredSelector({
      comments: selectAllComments,
    })
  );

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchComments(match.params.id));
  }, [dispatch, match.params.id]);
  return (
    <div className="comments">
      <h1>Post Comments</h1>
      <div className="comments-cards">
        {comments &&
          comments.map((comment) => {
            return <CardComment key={comment.id} comment={comment} />;
          })}
      </div>
    </div>
  );
};

export default withRouter(PostComments);
