import React from "react";
import "./commentCard.scss";
export const CardComment = ({ comment }) => {
  return (
    <div className="card-comment">
      <h3>{comment.email}</h3>
      <p>{comment.name}</p>
      <span>{comment.body}</span>
    </div>
  );
};
