import { createSelector } from "reselect";

export const selectPosts = (state) => state.post;

export const selectAllPosts = createSelector(
  [selectPosts],
  (post) => post.posts
);

export const selectAllComments = createSelector(
  [selectPosts],
  (post) => post.comments
);
