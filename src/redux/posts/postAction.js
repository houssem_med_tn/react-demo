import { postTypes } from "./postType";

export const fetchPosts = (id) => ({
  type: postTypes.FETCH_POSTS,
  payload: id,
});

export const fetchPostsSuccess = (posts) => ({
  type: postTypes.POST_SUCCESS,
  payload: posts,
});

export const postFailed = (error) => ({
  type: postTypes.FETCH_FAILURE,
  paylod: error,
});

export const fetchComments = (id) => ({
  type: postTypes.FETCH_COMMENTS,
  payload: id,
});

export const fetchCommentsSuccess = (comments) => ({
  type: postTypes.COMMENT_SUCCESS,
  payload: comments,
});

export const fetchCommentsFailure = (error) => ({
  type: postTypes.COMMENT_FAILED,
  payload: error,
});
