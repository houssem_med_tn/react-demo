import { postTypes } from "./postType";

const INITIAL_STATE = {
  posts: [],
  comments: [],
  id: "",
  error: "",
};
export const postReducers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case postTypes.FETCH_POSTS:
      return {
        ...state,
        id: action.payload,
      };
    case postTypes.POST_SUCCESS:
      return {
        ...state,
        eroor: "",
        posts: action.payload,
      };
    case postTypes.FETCH_FAILURE:
      return {
        ...state,
        posts: [],
        error: action.payload,
      };
    case postTypes.FETCH_COMMENTS:
      return {
        ...state,
        id: action.payload,
      };
    case postTypes.COMMENT_SUCCESS:
      return {
        ...state,
        comments: action.payload,
      };
    case postTypes.COMMENT_FAILED:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
