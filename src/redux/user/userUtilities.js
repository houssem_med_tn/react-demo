export const filterUserByName = (users, value) => {
  const excludedColumn = ["address", "company"];

  const filtered = users.filter((u) =>
    Object.keys(u).some((key) =>
      excludedColumn.includes(key)
        ? false
        : u[key].toString().toLowerCase().includes(value.toLowerCase())
    )
  );

  return { ...users, filtered };
};

const addUserTolist = (filtered, userToAdd) => {
  const existingUser = filtered.find((u) => u.id === userToAdd.id);

  if (existingUser) {
    return filtered.map((u) => (u.id === userToAdd.id ? { ...u } : u));
  }

  return [...filtered, { ...userToAdd }];
};
export default addUserTolist;
