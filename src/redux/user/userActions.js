import axios from "axios";
import Config from "../../config/config";
import { userTypes } from "./userTypes";

export const fetchAllUsers = () => {
  return {
    type: userTypes.FETCHALLUSERS,
  };
};

export const fetchSuccess = (users) => ({
  type: userTypes.FETCHSUCESS,
  payload: users,
});

export const fetchFailure = (error) => ({
  type: userTypes.FETCHFAILURED,
  payload: error,
});

export const clearItem = (user) => ({
  type: userTypes.CLEARITEM,
  payload: user,
});

export const setCurrentUser = (user) => ({
  type: userTypes.SETCURRENTUSER,
  payload: user,
});

export const addUserToListFavorit = (user) => ({
  type: userTypes.ADD_USER,
  payload: user,
});

export const clearItemFromFavoris = (user) => ({
  type: userTypes.CLEAR_ITEM_FROM_FAVORITES,
  payload: user,
});

export const filterByName = (text) => ({
  type: userTypes.FILTERBYNAME,
  payload: text,
});

export const getUser = (user) => ({
  type: userTypes.GETUSER,
  payload: user,
});

export const fecthAll = () => {
  return (dispatch) => {
    axios
      .get(`${Config.apiUrl}/users`)
      .then((res) => {
        const data = res.data;
        dispatch(fetchSuccess(data));
      })
      .catch((err) => {
        dispatch(fetchFailure(err));
      });
  };
};

// export const filterUserByName = (text) => {
//   return (dispatch) => {
//     axios
//       .get(`${Config.apiUrl}/users`)
//       .then((res) => {
//         const data = res.data;
//         dispatch(fetchSuccess(data));
//         dispatch(filterByName(text, data))
//       })
//       .catch((err) => {
//         dispatch(fetchFailure(err));
//       });
//   };
// };

export const getUserById = (id) => {
  return (dispatch) => {
    axios
      .get(`${Config.apiUrl}/users/${id}`)
      .then((res) => {
        const data = res.data;
        console.log(data);
        dispatch(getUser(data));
      })
      .catch((err) => {
        dispatch(fetchFailure(err));
      });
  };
};
