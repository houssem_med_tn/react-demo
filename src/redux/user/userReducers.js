import { userTypes } from "./userTypes";
import addUserTolist from "./userUtilities";
const INITIAL_STATE = {
  users: [],
  loading: true,
  error: "",
  userselected: null,
  value: "",
  filtered: [],
  currentUser: null,
};

const userReducers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case userTypes.SETCURRENTUSER:
      return {
        ...state,
        currentUser: action.payload,
      };
    case userTypes.FETCHALLUSERS:
      return {
        ...state,
        value: "",
        loading: true,
      };
    case userTypes.FETCHSUCESS:
      return {
        ...state,
        loading: false,
        value: "",
        users: action.payload,
        error: "",
      };
    case userTypes.FETCHFAILURED:
      return {
        ...state,
        loading: false,
        users: [],
        filtered: [],
        error: action.payload,
      };
    case userTypes.CLEARITEM:
      return {
        ...state,
        loading: false,
        users: state.users.filter((user) => user.id !== action.payload.id),
        error: "",
      };
    case userTypes.ADD_USER:
      return {
        ...state,
        filtered: addUserTolist(state.filtered, action.payload),
      };
    case userTypes.CLEAR_ITEM_FROM_FAVORITES:
      return {
        ...state,
        filtered: state.filtered.filter((u) => u.id !== action.payload.id),
      };
    case userTypes.FILTERBYNAME:
      return {
        value: action.payload,
        users: state.users,
        loading: false,
      };

    case userTypes.GETUSER: {
      return {
        ...state,
        loading: false,
        userselected: action.payload,
        users: state.users,
        error: "",
      };
    }
    default:
      return state;
  }
};

export default userReducers;
