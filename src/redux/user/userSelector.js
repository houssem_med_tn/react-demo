import { createSelector } from "reselect";

export const selectUser = (state) => state.user;

export const selectAllUser = createSelector([selectUser], (user) => user.users);

export const currentUser = createSelector(
  [selectUser],
  (user) => user.currentUser
);
export const selectFavoris = createSelector(
  [selectUser],
  (user) => user.filtered
);
export const selectUsers = createSelector(
  [selectUser],
  (user) => user.userselected
);

export const selectValue = createSelector([selectUser], (user) => user.value);
