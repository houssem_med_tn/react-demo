import { combineReducers } from "redux";
import { postReducers } from "./posts/postReducers";
import userReducers from "./user/userReducers";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
const rootReducers = combineReducers({
  user: userReducers,
  post: postReducers,
});

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["user"],
};
export default persistReducer(persistConfig, rootReducers);
