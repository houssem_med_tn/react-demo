import { createStore, applyMiddleware, compose } from "redux";
import rootReducers from "./rootReducers";
import Thunk from "redux-thunk";
import logger from "redux-logger";
import createSagaMiddlewares from "redux-saga";
import rootSaga from "../redux/sagas/index";
import { persistStore } from "redux-persist";
const sagaMiddleware = createSagaMiddlewares();
export const store = createStore(
  rootReducers,
  compose(
    applyMiddleware(sagaMiddleware, logger, Thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION__({ trace: true })
  )
);

export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);
export default { store, persistor };
