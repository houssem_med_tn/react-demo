import axios from "axios";
import { call, put, takeEvery } from "redux-saga/effects";
import Config from "../../config/config";
import { postTypes } from "../posts/postType";

function fetchApi(id) {
  return axios
    .get(`${Config.apiUrl}/posts?userId=` + id)
    .then((res) => res.data)
    .catch((err) => {
      throw err;
    });
}

function* fetchPosts(action) {
  const posts = yield call(fetchApi, action.payload);
  try {
    yield put({ type: postTypes.POST_SUCCESS, payload: posts });
  } catch (err) {
    yield put({ type: postTypes.FETCH_FAILURE, payload: err });
  }
}

function* postSaga() {
  yield takeEvery(postTypes.FETCH_POSTS, fetchPosts);
}

export default postSaga;
