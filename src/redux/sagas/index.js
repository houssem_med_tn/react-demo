import { all } from "redux-saga/effects";
import commentSaga from "./commentSagas.js";
import postSaga from "./postsSagas.js";
import userSagas from "./userSagas.js";

export default function* rootSaga() {
  yield all([userSagas(), postSaga(), commentSaga()]);
}
