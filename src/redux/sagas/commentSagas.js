import { call, put, takeEvery } from "redux-saga/effects";
import axios from "axios";
import Config from "../../config/config";
import { postTypes } from "../posts/postType";

function fetchApi(idPost) {
  return axios
    .get(`${Config.apiUrl}/comments?postId=` + idPost)
    .then((res) => res.data)
    .catch((err) => {
      throw err;
    });
}

function* fetchComments(action) {
  const comments = yield call(fetchApi, action.payload);
  try {
    yield put({ type: postTypes.COMMENT_SUCCESS, payload: comments });
  } catch (err) {
    yield put({ type: postTypes.COMMENT_FAILED, payload: err });
  }
}

function* commentSaga() {
  yield takeEvery(postTypes.FETCH_COMMENTS, fetchComments);
}

export default commentSaga;
