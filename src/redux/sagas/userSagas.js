import axios from "axios";
import { takeEvery, call, put } from "redux-saga/effects";
import Config from "../../config/config";
import { userTypes } from "../user/userTypes";

async function fetchApi() {
  return await axios
    .get(`${Config.apiUrl}/users`)
    .then((res) => res.data)

    .catch((err) => {
      throw err;
    });
}

function* fetchUsers(action) {
  try {
    const users = yield call(fetchApi);
    yield put({
      type: userTypes.FETCHSUCESS,
      payload: users,
      loading: false,
      value: "",
      error: "",
    });
  } catch (err) {
    yield put({ type: userTypes.FETCHFAILURED, error: "", loading: false });
  }
}

function* userSagas() {
  yield takeEvery(userTypes.FETCHALLUSERS, fetchUsers);
}

export default userSagas;
