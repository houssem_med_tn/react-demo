import React from "react";
import { useSelector } from "react-redux";
import { currentUser } from "./redux/user/userSelector";
import "./App.scss";
import Header from "./components/header/header";
import { Redirect, Route, Switch } from "react-router-dom";
import { LoginPage } from "./components/login/login";
import UserDe from "./components/user-details/userDetails";
import { Favoris } from "./components/favoris/favorite";
import postComments from "./components/comments/post-comment/postComments";
import HOME from "./components/home/home";
import { createStructuredSelector } from "reselect";

const App = () => {
  const { user } = useSelector(
    createStructuredSelector({
      user: currentUser,
    })
  );

  return (
    <div>
      <Header />
      <Switch>
        <Route
          exact
          path="/"
          render={() => (user !== null ? <HOME /> : <Redirect to="/login" />)}
        />
        <Route path="/users/:id" exact component={UserDe} />
        <Route path="/favorites" exact component={Favoris} />
        <Route path="/posts/:id" exact component={postComments} />
        <Route
          exact
          path="/login"
          render={() => (user === null ? <LoginPage /> : <Redirect to="/" />)}
        />
      </Switch>
    </div>
  );
};

export default App;
